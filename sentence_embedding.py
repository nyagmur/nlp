from flair.data import Sentence
from flair.embeddings import WordEmbeddings
from nltk.tokenize import sent_tokenize, word_tokenize
import statistics
import os
import re


def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return x


def embeddings_mean(sen):
    s = []
    stat = []
    l=0
    for l in range(len(sen)):
        sent = Sentence(sen[l])
        english_embedding.embed(sent)

        for token in sent:
            s.append(token.embedding)
        a = [float(n) for n in re.findall(r'-?\d+\.?\d*', str(s))]
        stat.append(statistics.mean(a))

    print(stat)
    return stat


# init embedding
english_embedding = WordEmbeddings('en')

f = open("creepy", "r")
creepy = f.read()
creepy_sent = sent_tokenize(creepy)
creepy_sent = remove_eol(creepy_sent)
print("creepy stat")
creepy_stat = embeddings_mean(creepy_sent)

f = open("gore", "r")
gore = f.read()
gore_sent = sent_tokenize(gore)
gore_sent = remove_eol(gore_sent)
print("gore stat")
gore_stat = embeddings_mean(gore_sent)


f = open("rage", "r")
rage = f.read()
rage_sent = sent_tokenize(rage)
rage_sent = remove_eol(rage_sent)
print("rage stat")
rage_stat = embeddings_mean(rage_sent)

f = open("happy", "r")
happy = f.read()
happy_sent = sent_tokenize(happy)
happy_sent = remove_eol(happy_sent)
print("happy stat")
happy_stat = embeddings_mean(happy_sent)






