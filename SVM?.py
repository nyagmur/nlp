from flair.data import Sentence
from flair.embeddings import WordEmbeddings
import numpy as np
from flair.embeddings import BertEmbeddings
from sklearn import svm
from nltk.tokenize import sent_tokenize, word_tokenize
import statistics
import os
import re


def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return x


def embeddings_mean(sen):
    s = []
    stat = []
    l=0
    for l in range(len(sen)):
        sent = Sentence(sen[l])
        english_embedding.embed(sent)

        for token in sent:
            s.append(token.embedding)
        a = [float(n) for n in re.findall(r'-?\d+\.?\d*', str(s))]
        stat.append(statistics.mean(a))

    print(stat)
    return stat


def class_type(classname, array):
    j = 0
    new_array = []  #array of classnames
    for j in range(len(array)):
        new_array.append(classname)
    return new_array




# init embedding
english_embedding = BertEmbeddings()

f = open("creepy", "r")
creepy = f.read()
creepy_sent = sent_tokenize(creepy)
creepy_sent = remove_eol(creepy_sent)
print("creepy stat")
creepy_stat = embeddings_mean(creepy_sent)

f = open("gore", "r")
gore = f.read()
gore_sent = sent_tokenize(gore)
gore_sent = remove_eol(gore_sent)
print("gore stat")
gore_stat = embeddings_mean(gore_sent)


f = open("rage", "r")
rage = f.read()
rage_sent = sent_tokenize(rage)
rage_sent = remove_eol(rage_sent)
print("rage stat")
rage_stat = embeddings_mean(rage_sent)


f = open("happy", "r")
happy = f.read()
happy_sent = sent_tokenize(happy)
happy_sent = remove_eol(happy_sent)
print("happy stat")
happy_stat = embeddings_mean(happy_sent)


i = 0

class_array = []
class_array = class_type("1", creepy_stat)
# x= class names, labels
x = np.concatenate((class_array, class_type("2", gore_stat)), axis= None)
x = np.concatenate((x, class_type("3", rage_stat)), axis= None)
x = np.concatenate((x, class_type("4", happy_sent)), axis= None)
#class_array.append(str(class_type("3", rage_stat)))
#class_array.append(str(class_type("4", happy_stat)))

print("new_array:")
print(x)
print("MySet")
myset=[]
creepy_stat.append(gore_stat)
creepy_stat.append(rage_stat)
creepy_stat.append(happy_sent)
X = myset  # word embeddings

y = x  # class labels
lin_clf = svm.LinearSVC()
lin_clf.fit(X, y)
svm.LinearSVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovo', degree=3, gamma='scale', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)
dec = lin_clf.decision_function([0.02])
print(dec.shape([0.02]))

