import nltk
import re
import matplotlib.pyplot as plt

# regex istisnai durumlar için (tarih veya maddeleri açıklayan kelimeler, 12.02.2019 veya 12.5 madde gibi)

def match_and_remove(mydata):

  x = re.search("[0-9]*[.,][0-9]*", mydata)
  if (x):

    re.sub('[0-9]*[.,][0-9]*','', mydata)


# regex istisnai durumlar için (kısaltmalar)

def match_and_remove_abb(mydata):

  y = re.search("[A-Z]*[.,][A-Z]*", mydata)
  if (y):

    re.sub('[A-Z]*[.,][A-Z]*', '', mydata)



myList = []
count_what = []
number_of_occurances = []
text_file= open("/home/nihal/PycharmProjects/nlp/venv/gsi_sozlesmeler.txt", 'r')
#data = "12.5. KİRACI’nın isteği üzere bozulma ile ilgili servisi verir. KİRAYA VEREN, KİRACI’nın arıza bildiriminden [●]....... çalışma saati içerisinde mahalle ulaşır ve arızayı gidermek için elinden gelen çabayı gösterir. - Malların geçici olarak ikame edilmesini ücretsiz olarak sağlar."
#data= "(Ref. No:20) Barter SözleşmesiCine Müzik TV Radyo Yay Yap ve Rek. A.Ş.02.04.2009"
#data = "İşbu sözleşme tarafımıza temin edilmediği için DD raporu kapsamında incelenememiştir.(Ref. No:21) Barter SözleşmesiVatan Dergi Grubu A.Ş. Cine Müzik TV Radyo Yay Yap ve Rek. A.Ş.Sözleşme Süresi Bitti/Cari Hesap İlişkisi Devam Ediyor"
data=text_file.read()
match_and_remove(data)
match_and_remove_abb(data)
listOfSentences = data.split(".")

ı=0
for i in range(len(listOfSentences)):
  res = len(re.findall(r'\w+', listOfSentences[i]))
  myList.append(res)




for i in myList:
  print(i)  # counts 1's for instance  -- kac cumlenin 2 uzunlugunda oldugunu anlama calısıyoruz
  count_what.append(i)

  temp= myList.count(i)
  number_of_occurances.append(temp)

f = open("istatistik.txt", "x")
f.write("Her bir cümlede saydığı uzunluk: ")
f.write("\n")
f.write(str(count_what))
f.write("\n\n")
f.write("O uzunlukta kaç cümle var?")
f.write("\n")
f.write(str(number_of_occurances))
f.close