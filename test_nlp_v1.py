import re


#15. madde veya 12.02.2019 gibi tarihler için regex

def match_and_remove(mydata):

  x = re.search("[0-9]*[.,][0-9]*", mydata)
  if (x):
    print("YES! We have a match!")
    re.sub('[0-9]*[.,][0-9]*','', mydata)
  else:
   print("No match")

  return mydata
  
# kısaltmalarla ilgili problemlerin çözümü için

def match_and_remove_abb(mydata):

  y = re.search("[A-Z]*[.,][A-Z]*", mydata)
  if (y):
    print("YES! We have a match!")
    re.sub('[A-Z]*[.,][A-Z]*', '', mydata)
  else:
    print("No match")

  return mydata


myList = []
count_what = []
number_of_occurances = []
text_file= open("/home/nihal/PycharmProjects/nlp/venv/gsi_sozlesmeler.txt", 'r')
#data = "12.5. KİRACI’nın isteği üzere bozulma ile ilgili servisi verir. KİRAYA VEREN, KİRACI’nın arıza bildiriminden [●]....... çalışma saati içerisinde mahalle ulaşır ve arızayı gidermek için elinden gelen çabayı gösterir. - Malların geçici olarak ikame edilmesini ücretsiz olarak sağlar."
#data= "(Ref. No:20) Barter SözleşmesiCine Müzik TV Radyo Yay Yap ve Rek. A.Ş.02.04.2009"
data = "İşbu sözleşme tarafımıza temin edilmediği için DD raporu kapsamında incelenememiştir.(Ref. No:21) Barter SözleşmesiVatan Dergi Grubu A.Ş. Cine Müzik TV Radyo Yay Yap ve Rek. A.Ş.Sözleşme Süresi Bitti/Cari Hesap İlişkisi Devam Ediyor"
#data=text_file.read()
data= match_and_remove(data)
data = match_and_remove_abb(data)
listOfSentences = data.split(".")
print(listOfSentences[0])

print(listOfSentences[2])

print(listOfSentences)
ı=0
for i in range(len(listOfSentences)):
  res = len(re.findall(r'\w+', listOfSentences[i]))
  myList.append(res)
  print(res)



for i in myList:
  print("counts of  :")
  print(i)  # counts 1's for instance  -- kac cumlenin 2 uzunlugunda oldugunu anlama calısıyoruz
  count_what.append(i)
  print("is")
  temp= myList.count(i)
  print(temp) # print: how many occurrances of i (1) exist.   kac cumle bir uzunlugundadir sorusunun cevabı
  number_of_occurances.append(temp)


print(count_what)
print(number_of_occurances)