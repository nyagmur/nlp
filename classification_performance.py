from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
y_true = [2, 0, 2, 2, 0, 1]
y_pred = [0, 0, 2, 2, 0, 2]

a = confusion_matrix(y_true, y_pred)

print("confusion matrix:")
print(a)

print("accuracy score: ")

print(accuracy_score(y_true, y_pred))

print("recall:")
print(recall_score(y_true, y_pred, average='weighted'))


print("classification report:")
target_names = ['class 0', 'class 1', 'class 2']
print(classification_report(y_true, y_pred, target_names = target_names))