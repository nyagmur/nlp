from flair.data import Sentence
from flair.embeddings import WordEmbeddings
from nltk.tokenize import sent_tokenize, word_tokenize
import statistics
import os
import re

# init embedding
english_embedding = WordEmbeddings('en')

f = open("creepy", "r")
creepy = f.read()

f = open("gore", "r")
gore = f.read()

f = open("rage", "r")
rage = f.read()

f = open("happy", "r")
happy = f.read()

creepy_sent = sent_tokenize(creepy)
gore_sent = sent_tokenize(gore)
rage_sent = sent_tokenize(rage)
happy_sent = sent_tokenize(happy)


x=[]
# cümle sonunda nokta yoksa, alt satıra geçip diğer cümle ile birleştiriyor. Bu nedenle \n kısımlarını eliminate ettim.
for i in creepy_sent:
#    x.append(i.rstrip('\n'))
     x.append(i.replace('\n', ' '))
print(creepy_sent)
print(x)

l = 0
s= []
stat= []

for l in range(len(creepy_sent)):
    sent = Sentence(creepy_sent[l])
    english_embedding.embed(sent)

    for token in sent:
       s.append(token.embedding)
    a = [float(n) for n in re.findall(r'-?\d+\.?\d*', str(s))]
    stat.append(statistics.mean(a))

print(stat)
#print(creepy_sent)

emd = []

# for x in creepy_sent:
  #  emd.append(x.embedding)

print(emd)