import numpy as np

from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
import pandas as pd

df = pd.read_csv('breast-cancer-wisconsin.data.txt')
df.replace('?',-99999, inplace=True)

X = np.array(df.drop(['class'], 1))
y = np.array(df['class'])

# X  data
# y  labellar
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)

clf = svm.SVC()

clf.fit(X_train, y_train)
confidence = clf.score(X_test, y_test)
print(confidence)

predicted = clf.predict(X_test)

print("accuracy_score:")
print(accuracy_score(y_test, predicted))

a = confusion_matrix(y_test, predicted)
# print("confusion matrix")

print("summary_score:")
print((classification_report(y_test, predicted)))

example_measures = np.array([[111111,4,2,1,1,1,2,3,2,1]])
example_measures = example_measures.reshape(len(example_measures), -1)
prediction = clf.predict(example_measures)
print(prediction)