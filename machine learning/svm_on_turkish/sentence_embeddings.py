import csv
from fastText import FastText
import numpy as np

from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
import pandas as pd

with open("/home/nihal/PycharmProjects/csvread-turkishsvm/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)
print(d[1][0]) # 248
print(d[1][1])
print(len(d))

def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return (''.join(x))

model = FastText.load_model('wiki.tr.bin')
embedding_list=[]
label_list = []
for i in range(1,1000):
#for i in range(1,(len(d))):
#    print(i,len(d))
    print(remove_eol(d[i][1]))
    vec1 = model.get_sentence_vector(remove_eol(d[i][1]))
    embedding_list.append(vec1)
    vec2 = (int(d[i][0]))
    label_list.append(vec2)
  #  print(vec2, vec1)

print(embedding_list)
print(label_list)

X_train, X_test, y_train, y_test = model_selection.train_test_split(embedding_list, label_list, test_size=0.2, random_state=40)

clf = svm.SVC(gamma='auto')

clf.fit(X_train, y_train)
confidence = clf.score(X_test, y_test)
print(confidence)

predicted = clf.predict(X_test)

print("accuracy_score:")
print(accuracy_score(y_test, predicted))

a = confusion_matrix(y_test, predicted)
# print("confusion matrix")

print("summary_score:")
print((classification_report(y_test, predicted)))


