from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import statistics
import numpy as np
from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
import pandas as pd
import csv
from fastText import FastText

with open("/path/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)
print(d[1][0]) # 248
print(d[1][1])
print(len(d))

def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return (''.join(x))

model = FastText.load_model('wiki.tr.bin')
embedding_list=[]
label_list = []
for i in range(1,1000):
#for i in range(1,(len(d))):
#    print(i,len(d))
    vec1 = model.get_sentence_vector(remove_eol(d[i][1]))
    embedding_list.append(vec1)
    vec2 = (int(d[i][0]))
    label_list.append(vec2)
# list of text documents
#text = ["ELma, armut yiyorum.",
#       "Elma al.",
 #       "Elmayı ver"]
my_list= []
for i in range(1,1000):
    x=remove_eol(d[i][1])
    my_list.append(x)

arr= my_list
print(arr)

# If you want to take into account just term frequencies:
vectorizer = CountVectorizer(ngram_range=(2,2))
# The ngram range specifies your ngram configuration.

X = vectorizer.fit_transform(arr)
# Testing the ngram generation:
print(vectorizer.get_feature_names())
# This will print: ['by car', 'by jack', 'car was', 'cleaned by', 'jack was', 'was cleaned']

print(X.toarray())
# This will print:[[0 1 1 1 0 1], [1 0 0 1 1 1]]

## And now testing TFIDF vectorizer:
vectorizer = TfidfVectorizer(ngram_range=(2,2)) # You can still specify n-grams here.
X = vectorizer.fit_transform(arr)


# Testing the TFIDF value + ngrams:
print(X.toarray())
# This will print:  [[ 0.          0.57615236  0.57615236  0.40993715  0.          0.40993715]
# [ 0.57615236  0.          0.          0.40993715  0.57615236  0.40993715]]


## Testing TFIDF vectorizer without normalization:
vectorizer = TfidfVectorizer(ngram_range=(2,2), norm=None) # You can still specify n-grams here.
X = vectorizer.fit_transform(arr)

# Testing TFIDF value before normalization:
print(X.toarray())
# This will print: [[ 0.          1.40546511  1.40546511  1.          0.          1.        ]
# [ 1.40546511  0.          0.          1.          1.40546511  1.        ]]

X_train, X_test, y_train, y_test = model_selection.train_test_split(X, label_list, test_size=0.2, random_state=40)

clf = svm.SVC(gamma='auto')

clf.fit(X_train, y_train)
confidence = clf.score(X_test, y_test)
print(confidence)

predicted = clf.predict(X_test)

print("accuracy_score:")
print(accuracy_score(y_test, predicted))

a = confusion_matrix(y_test, predicted)
# print("confusion matrix")

print("summary_score:")
print((classification_report(y_test, predicted)))
