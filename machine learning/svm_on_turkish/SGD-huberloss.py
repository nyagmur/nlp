from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer,TfidfTransformer

import re

from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline

import csv

from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report

with open("filepath/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)

def remove_eol(s):
 x=[]
 for i in s:
   x.append(i.replace('\n', ' '))

 return (''.join(x))

label_list = []
sentences = []
for i in range(1,(len(d))):
    sent = remove_eol(d[i][1])
    sentences.append(sent)
    vec2 = (int(d[i][0]))
    label_list.append(vec2)

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = ['ve', 'veya', 'herhangibir']

X = sentences
y = label_list
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state = 42)


sgd = Pipeline([('vect', CountVectorizer()),
                ('clf', SGDClassifier(loss='modified_huber', penalty='l2',alpha=1e-3, random_state=42, max_iter=1000, tol=None)),
               ])
sgd.fit(X_train, y_train)

y_pred = sgd.predict(X_test)

print('accuracy %s' % accuracy_score(y_pred, y_test))
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))