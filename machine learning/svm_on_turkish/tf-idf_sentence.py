from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
import pandas as pd
import csv
from fastText import FastText

with open("/filepath/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)
print(d[1][0]) # 248
print(d[1][1])
print(len(d))

def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return (''.join(x))

model = FastText.load_model('wiki.tr.bin')
embedding_list=[]
label_list = []
for i in range(1,1000):
#for i in range(1,(len(d))):
#    print(i,len(d))
    vec1 = model.get_sentence_vector(remove_eol(d[i][1]))
    embedding_list.append(vec1)
    vec2 = (int(d[i][0]))
    label_list.append(vec2)
# list of text documents
#text = ["ELma, armut yiyorum.",
#       "Elma al.",
 #       "Elmayı ver"]
my_list= []
for i in range(1,1000):
    x=remove_eol(d[i][1])
    my_list.append(x)

text= my_list

# create the transform
vectorizer = TfidfVectorizer()
# tokenize and build vocab
vectorizer.fit(text)
# summarize
print(vectorizer.idf_)
# encode document
vector = vectorizer.transform([text[0]])
# summarize encoded vector
print(vector.shape)
print(vector.toarray())

print(vectorizer.vocabulary_.items())
# print(vectorizer.vocabulary_.pop('elma'))

# new_label=[]
# for i in range(1,1000):
  #  x=remove_eol(d[i][1])
  #  if (x.find(vectorizer.vocabulary_.items())!=-1)
   #     new_label.append("1")
    #    else
     #   new_label.append("0")


