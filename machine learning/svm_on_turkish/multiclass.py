from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer,TfidfTransformer
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import re
from bs4 import BeautifulSoup
#matplotlib inline
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline

import csv
from fastText import FastText
import numpy as np

from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report
import pandas as pd

from sklearn import preprocessing, model_selection, neighbors, svm
from sklearn.metrics import confusion_matrix, accuracy_score, recall_score, precision_score, classification_report

with open("/filepath/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)
#df = pd.read_csv('stack-overflow-data.csv')

def remove_eol(s):
 x=[]
 for i in s:
        #    x.append(i.rstrip('\n'))
   x.append(i.replace('\n', ' '))

 return (''.join(x))

model = FastText.load_model('wiki.tr.bin')
embedding_list=[]
label_list = []
sentences = []
#for i in range(1,1000):
for i in range(1,(len(d))):
#    print(i,len(d))
#    print(remove_eol(d[i][1]))
    sent = remove_eol(d[i][1])
    sentences.append(sent)
    vec1 = model.get_sentence_vector(sent)
    embedding_list.append(vec1)
    vec2 = (int(d[i][0]))
    label_list.append(vec2)

#print(df['post'].apply(lambda x: len(x.split(' '))).sum())

#my_tags = ['java','html','asp.net','c#','ruby-on-rails','jquery','mysql','php','ios','javascript','python','c','css','android','iphone','sql','objective-c','c++','angularjs','.net']


REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = ['ve', 'veya', 'herhangibir']


X = sentences
y = label_list
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state = 42)


sgd = Pipeline([('vect', CountVectorizer()),
                ('tfidf', TfidfTransformer()),
                ('clf', SGDClassifier(loss='hinge', penalty='l2',alpha=1e-3, random_state=42, max_iter=5, tol=None)),
               ])
sgd.fit(X_train, y_train)

y_pred = sgd.predict(X_test)

print('accuracy %s' % accuracy_score(y_pred, y_test))
print(classification_report(y_test, y_pred))