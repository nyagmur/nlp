from numpy import asarray
from numpy import zeros
import numpy as np
from keras.callbacks import EarlyStopping
import csv
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from keras.layers import Dense, Embedding, LSTM, Bidirectional, SpatialDropout1D, Flatten, TimeDistributed, Input
from keras import Model

from keras.models import Sequential
from keras.utils.np_utils import to_categorical

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import re

with open("/home/nihal/PycharmProjects/LSTM-final/fileread.txt") as f:
    reader = csv.reader(f, delimiter="\t")
    d = list(reader)

num_of_categories = 45000

n_most_common_words = 8000
max_len = 130

def remove_eol(s):
 x=[]
 for i in s:
   x.append(i.replace('\n', ' '))
 return (''.join(x))


labels = []
labels2 = []
labels3 = []

docs = []
docs2 = []
docs3 = []

#length= len(d)
length = 256
for i in range(1, length):

 if i< (length-2):
    sent = remove_eol(d[i][1])
    docs.append(sent)
    lbl = (int(d[i][0]))
    labels.append(lbl)
    sent2 = remove_eol(d[i+1][1])
    docs2.append(sent2)
    lbl2 = (int(d[i+1][0]))
    labels2.append(lbl2)
    sent3 = remove_eol(d[i+2][1])
    docs3.append(sent3)
    lbl3 = (int(d[i+2][0]))
    labels3.append(lbl3)

X = docs
y = labels

X2 = docs2
y2 = labels2
# prepare tokenizer

X3 = docs3
y3 = labels3

label_names = []


#print(grouped_sentences)
#sentences = re.split(r' *[\.\?!][\'"\)\]]* *', docs)
"""
t = Tokenizer()
t.fit_on_texts(docs)

vocab_size = len(t.word_index) + 1
# integer encode the documents

encoded_docs = t.texts_to_sequences(docs)
print(encoded_docs)
# pad documents to a max length of 4 words
max_length = 130
padded_docs = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
print(padded_docs)
# load the whole embedding into memory
embeddings_index = dict()
f = open('cc.tr.300.vec')

for line in f:
	values = line.split()
	word = values[0]
	coefs = asarray(values[1:], dtype='float32')
	embeddings_index[word] = coefs
f.close()

print('Loaded %s word vectors.' % len(embeddings_index))

# create a weight matrix for words in training docs
embedding_matrix = zeros((vocab_size, 300))
for word, i in t.word_index.items():
	embedding_vector = embeddings_index.get(word)
	if embedding_vector is not None:
		embedding_matrix[i] = embedding_vector

# define model
X_train, X_test, y_train, y_test = train_test_split(padded_docs , labels, test_size=0.25, random_state=42)

input = Input(shape=(max_len,))

# dimension of the embedding vector: 300

model = Embedding(vocab_size,300,weights=[embedding_matrix],input_length=max_len)(input)
#500 memory units(nodes)
model = Bidirectional (LSTM (500,return_sequences=True,dropout=0.01),merge_mode='concat')(model)
model = TimeDistributed(Dense(237,activation='relu'))(model)
model = Flatten()(model)  #lstm modeli yerine kullanılabilir.
model = Dense(237,activation='relu')(model)
output = Dense(231,activation='softmax')(model)  #237 öncesiydi.
model = Model(input,output)
model.compile(loss='categorical_crossentropy',optimizer='adam', metrics=['accuracy'])

#model.fit(X_train,to_categorical(y_train),validation_split=0.25, epochs = 10, verbose = 2)
model.fit(X_train, to_categorical(y_train), epochs=10, batch_size=20,validation_split=0.2,callbacks=[EarlyStopping(monitor='val_loss',patience=7, min_delta=0.0001)])
# fit the model
#model.fit(X_train, to_categorical(y_train), epochs=20, verbose=0)
# evaluate the model

loss, accuracy = model.evaluate(X_test, to_categorical(y_test), verbose=0)
print('Accuracy: %f' % (accuracy))


#accr = model.evaluate(X_test,to_categorical(y_test))
#print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))
"""

t2 = Tokenizer()
t2.fit_on_texts(docs2)

vocab_size2 = len(t2.word_index) + 1
# integer encode the documents

encoded_docs2 = t2.texts_to_sequences(docs2)
print(encoded_docs2)
# pad documents to a max length of 4 words
max_length2 = 130
padded_docs2 = pad_sequences(encoded_docs2, maxlen=max_length2, padding='post')
print(padded_docs2)
# load the whole embedding into memory
embeddings_index2 = dict()
f = open('cc.tr.300.vec')

for line in f:
	values = line.split()
	word = values[0]
	coefs = asarray(values[1:], dtype='float32')
	embeddings_index2[word] = coefs
f.close()

print('Loaded %s word vectors.' % len(embeddings_index2))

# create a weight matrix for words in training docs
embedding_matrix2 = zeros((vocab_size2, 300))
for word, i in t2.word_index.items():
	embedding_vector2 = embeddings_index2.get(word)
	if embedding_vector2 is not None:
		embedding_matrix2[i] = embedding_vector2

# define model
X_train2, X_test2, y_train2, y_test2 = train_test_split(padded_docs2 , labels2, test_size=0.2, random_state=42)

input2 = Input(shape=(max_len,))

# dimension of the embedding vector: 300

model2 = Embedding(vocab_size2,300,weights=[embedding_matrix2],input_length=max_len)(input2)
#500 memory units(nodes)
model2 = Bidirectional (LSTM (500,return_sequences=True,dropout=0.01),merge_mode='concat')(model2)
model2 = TimeDistributed(Dense(237,activation='relu'))(model2)
model2 = Flatten()(model2)  #lstm modeli yerine kullanılabilir.
model2 = Dense(237,activation='relu')(model2)
output2 = Dense(231,activation='softmax')(model2)  #237 öncesiydi.
model2 = Model(input2,output2)
model2.compile(loss='categorical_crossentropy',optimizer='adam', metrics=['accuracy'])

#model.fit(X_train,to_categorical(y_train),validation_split=0.25, epochs = 10, verbose = 2)
model2.fit(X_train2, to_categorical(y_train2), epochs=10, batch_size=20,validation_split=0.2,callbacks=[EarlyStopping(monitor='val_loss',patience=7, min_delta=0.0001)])
# fit the model
#model.fit(X_train, to_categorical(y_train), epochs=20, verbose=0)
# evaluate the model

loss, accuracy = model2.evaluate(X_test2, to_categorical(y_test2), verbose=0)
print('Accuracy: %f' % (accuracy))

#accr = model.evaluate(X_test,to_categorical(y_test))
#print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))

