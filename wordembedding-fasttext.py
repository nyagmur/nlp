from flair.data import Sentence
from flair.embeddings import WordEmbeddings

# init embedding
turkish_embedding = WordEmbeddings('tr')

# create sentence.
sentence = Sentence("Kiracı her ay kirayı ödemekle yükümlüdür")

# embed a sentence using glove.
turkish_embedding.embed(sentence)

# now check out the embedded tokens.
for token in sentence:
    print(token)
    print(token.embedding)